#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	int taille = toSort.size() ; 
	int j ; 
	Array& result=w->newArray(taille);

	result[0] = toSort[0] ; 
	 for(int i=1; i<taille; i++){ 
            for(j=0; j<i; j++){ //tant que j inférieur à 
                if(result[j]>toSort[i]){ //Si la valeur de result est supérieure à celle de toSort 
                    break; //On sort de la boucle for
                }
            }
            result.insert(j,toSort[i]); //insert valeur de toSort à la place j et décale les valeurs d'après 
        }

	toSort=result; // update the original array
}



int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
