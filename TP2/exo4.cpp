#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
	if(size<=1){
		return ; //si la taille du tableau est = 1 le tri est terminé 
	}

	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0; // effectives sizes

	// split
	int pivot=toSort[0];
    for(int i=1;i<size;i++){
        if(toSort[i]<pivot){ //si c'est plus petit que le pivot --> tableau des petites valeurs 
            lowerArray[lowerSize]=toSort[i];
            lowerSize++; 
        }
        else{ //si c'est plus grand que le pivot --> tableau des grandes valeurs 
            greaterArray[greaterSize]=toSort[i];
            greaterSize++;
        }
     }

	// recursiv sort of lowerArray and greaterArray
        recursivQuickSort(greaterArray,greaterSize); 
        recursivQuickSort(lowerArray,lowerSize);

	// merge
    for(int i=0;i<lowerSize;i++){
		toSort[i]=lowerArray[i]; //on insert le tableau des petites valeurs 
	}

	toSort[lowerSize]=pivot; // on place le pivot entre les deux tableaux

    for(int i=1;i<=greaterSize;i++){
        toSort[lowerSize+i]=greaterArray[i-1]; //on insert le tableau des grandes valeurs 
    }
}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size()); //appel à la fonction la première fois 
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
