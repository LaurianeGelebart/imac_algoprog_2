#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void bubbleSort(Array& toSort){
	int taille = toSort.size() ;
	bool noswap = false ; 
	while (!noswap){ //on recommence tant qu'on fait des swap 
		noswap = true ; 
		for (int i=0; i<taille; i++){
			if(toSort[i]>toSort[i+1]){ //si le voisin est plus petit que l'élément
				toSort.swap(i, i+1); //on swap 
				noswap = false ; //il y a un swap 
			}
		}
	}
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
