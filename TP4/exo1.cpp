#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    return nodeIndex * 2 + 1;
}

int Heap::rightChild(int nodeIndex)
{
    return nodeIndex * 2 + 2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
	this->get(i) = value ; 
	while ( i>0 && this->get(i)>this->get((i-1)/2) )
	{
		this->swap(i, ((i-1)/2) ) ;
		i = (i-1)/2 ;
	}
	
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i_max = nodeIndex;
	if(leftChild(nodeIndex) < heapSize && this->get(leftChild(nodeIndex)) > this->get(i_max)){
		i_max = leftChild(nodeIndex); 
	}
	if(rightChild(nodeIndex) < heapSize && this->get(rightChild(nodeIndex)) > this->get(i_max)){
		i_max = rightChild(nodeIndex); 
	}
	
	if(i_max != nodeIndex){
		this->swap(i_max, nodeIndex); 
		heapify(heapSize, i_max) ; 
	}
}

void Heap::buildHeap(Array& numbers)
{
	int n = numbers.size(); 
	for (int i = 0; i < n; i++)
	{
		insertHeapNode(n, numbers[i]); 
	}
}

void Heap::heapSort()
{
	int n = this->size();
	for (int i=n-1; i>=0; i--) {
		this->swap(0, i) ; 
		heapify(i, 0); 
	}
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
